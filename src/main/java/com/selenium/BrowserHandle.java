package com.selenium;

import java.net.MalformedURLException;
import java.net.URL;

import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class BrowserHandle {
	@Test(testName = "Browser Methods")
	
	public void seleniumMethods()
	// Create ChromeDriver object

	{	System.out.println(System.getProperty("user.dir"));  //-->Prints the absolute part
		System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + "\\driver\\chromedriver.exe");
		
		// This step is necessary for cross browser testing.The reference wd can be used to call any driver.
		//WebDirver is an interface  and it extends an interface called "Search context". WebElement is another interface and it implements
		//the Search Context.  "Search Context" provides us with two important METHODS, 1.find element and 2.find elements.
		
		//All locators are "static methods" and belong to the class "By"
		
		WebDriver wd = new ChromeDriver(); 
		
		//Launch the browser
		wd.get("https://www.suvideals.ooo/");
		
		//Dimension size = new Dimension(10,200);
		//wd.manage().window().setSize(size);
		wd.manage().window().maximize();
		
		String currenturl = wd.getCurrentUrl(); //--> Gets and prints current url
		System.out.println(currenturl);
		
		String title = wd.getTitle();//-->Gets and prints the page title 
		System.out.println(title);
		
		wd.navigate().refresh(); //-->Refreshes the window
		
		//Opens up another handle
		URL url = null;
		try {
			url = new URL("https://google.com");
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		wd.navigate().to(url);
		
		wd.navigate().back();
		
		
		
		//Close or quit
		//wd.close();
		//wd.quit();
	
	}
}
