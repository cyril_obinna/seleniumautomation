package com.selenium;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class NavigationHandle {
	private WebDriver wd;

	@BeforeMethod
	public void setup() {
		System.out.println(System.getProperty("user.dir")); // -->Prints the absolute part
		System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + "\\driver\\chromedriver.exe");
		wd = new ChromeDriver();

	}

	@Test(testName = "navigate handle")
	public void navigationMethod() throws InterruptedException {  
		wd.get("https://www.google.com");
		wd.navigate().to("https://www.facebook.com");
		wd.navigate().back();
		Thread.sleep(2000);
		wd.navigate().forward();
	}
}
