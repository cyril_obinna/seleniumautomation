package com.selenium;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class LoginPageTest {

	private WebDriver wd;

	@BeforeMethod
	public void setup() {
		System.out.println(System.getProperty("user.dir")); // -->Prints the absolute part
		System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + "\\driver\\chromedriver.exe");
		wd = new ChromeDriver();
	}

	@Test(testName = "login method", enabled = false )

	public void loginMethod() throws InterruptedException {

		wd.get("https://www.suvideals.ooo/");
		wd.manage().window().maximize();
		
		WebElement loginLink = wd.findElement(By.partialLinkText("LOGIN"));
		Thread.sleep(2000);
		loginLink.click();
		
		Thread.sleep(1000);
		WebElement emailTextbox = wd.findElement(By.id("userHandle"));
		emailTextbox.sendKeys("jvsharma18@gmail.com");
		
		Thread.sleep(1000);
		WebElement passwordTextbox = wd.findElement(By.id("password"));
		passwordTextbox.sendKeys("Qweqwe2@");
		
		Thread.sleep(1000);
		WebElement LoginTab = wd.findElement(By.xpath("//button[@class='btn btn-login']"));
		LoginTab.click();
		
		//To verify user is logged in
		WebElement isLoggedInLink = wd.findElement(By.xpath("//*[@href='/jsp/logout.jsp']/../a"));
		String actualResult = isLoggedInLink.getText();
		System.out.println(actualResult);
		Assert.assertEquals(actualResult, "HELLO JATIN");
	}
	
	@Test(testName="registration")
	public void registrationTest() throws InterruptedException
	{
		wd.get("http://www.suvideals.ooo/");
		wd.manage().window().maximize();
		
		WebElement loginLink = wd.findElement(By.partialLinkText("SIGN UP"));
		Thread.sleep(1000);
		loginLink.click();
		
		Thread.sleep(1000);
		WebElement registrationTab = wd.findElement(By.id("new-account-btn"));
		registrationTab.click();
		Thread.sleep(1000);
		
		WebElement userNameTextBox = wd.findElement(By.xpath("//input[@name='userName']"));
		userNameTextBox.sendKeys("Cyril");
		
		WebElement emailTextBox = wd.findElement(By.name("email"));
		emailTextBox.sendKeys("obinna284@gmail.com");
		
		//Since there are two fields with same attributes, we chain the webElements
		WebElement registrationForm = wd.findElement(By.id("registration-form"));
		WebElement password = registrationForm.findElement(By.id("password"));
		password.sendKeys("excels123");
		
		Thread.sleep(1000);
		WebElement createAccount = registrationForm.findElement(By.id("register-button-link"));
		createAccount.click();
		
	}	
	
	//@AfterMethod
	
}

//driver.find_element_by_xpath("//*[@class='panelTrigger']").click()










